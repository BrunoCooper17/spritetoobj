'''
MIT License

Copyright (c) 2019 Jesus Mastache Caballero (BrunoCooper17)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 '''

from functools import partial
from PyQt5 import QtWidgets, QtGui
#from PyQt5.QtWidgets import QFileDialog, QMessageBox, QLabel, QScrollArea, QWidget, QVBoxLayout, QPushButton, QFormLayout, QHBoxLayout
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
from AppInterface import Ui_MainWindow  # importing our generated file

import sys
import os.path
import subprocess
import fnmatch
import re
import array
import json

class mywindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.ButtonAdd.clicked.connect(self.AddFile);
        self.ui.ButtonConvert.clicked.connect(self.ConvertEverything);

        self.MyFont = QtGui.QFont()
        self.MyFont.setFamily("DejaVu Sans Mono")

        #Container Widget        
        self.widget = QtWidgets.QWidget()
        #Layout of Container Widget
        self.verticalLayout = QtWidgets.QFormLayout()
        self.widget.setLayout(self.verticalLayout)

        self.ui.scrollArea.setWidgetResizable(True)
        self.ui.scrollArea.setWidget(self.widget)

        self.ArrayFileName = []
        self.ArrayHBLayout = []

        if os.path.isfile(".confApkPackager"):
            print("Exist!")
            configRead = open(".confApkPackager")
            self.LastDirection = configRead.readline()
        else:
            print("Fail!")
            self.LastDirection = "/"
            self.SaveConfig(self.LastDirection)


    def SaveConfig(self, LastDir):
        configRead = open(".confApkPackager", "w")
        configRead.write(LastDir)
        configRead.close()


    def AddFile(self):
        dir = QtWidgets.QFileDialog.getOpenFileNames(None, 'Select a file(s):', self.LastDirection, "SpriteSheet (*.json)")
        for file in dir[0]:
            self.AddFileLabel(file)
            self.LastDirection = os.path.dirname(file)
        self.SaveConfig(self.LastDirection)


    def AddFileLabel(self, FileStr):
        btn = QtWidgets.QPushButton("Remove")
        btn.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        btn.clicked.connect(partial(self.ButtonRemovePress, btn))
        btn.setFont(self.MyFont)

        LabelFile = QtWidgets.QLabel(FileStr)
        LabelFile.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        LabelFile.setFont(self.MyFont)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(LabelFile)
        hLayout.addWidget(btn)
        btn.setProperty("Parent", hLayout)

        self.verticalLayout.addRow(hLayout)

        self.ArrayFileName.append(FileStr)
        self.ArrayHBLayout.append(hLayout)
    

    def ButtonRemovePress(self, button):
        indexRemove = self.ArrayHBLayout.index(button.property("Parent"))
        self.ArrayFileName.remove(self.ArrayFileName[indexRemove])
        self.ArrayHBLayout.remove(self.ArrayHBLayout[indexRemove])
        self.verticalLayout.removeRow(indexRemove)


    def ConvertEverything(self):
        for filename in self.ArrayFileName:
            self.ConvertFile(filename)

        for i in reversed(range(self.verticalLayout.count())):
            self.verticalLayout.removeRow(i)

        self.ArrayFileName.clear()
        self.ArrayHBLayout.clear()


    def RotateVertexXY(self, Vertex):
        tmpValue = Vertex[0]
        Vertex[0] = -Vertex[2]
        Vertex[2] = tmpValue
        return


    def RotateVertexXZ(self, Vertex):
        tmpValue = Vertex[0]
        Vertex[0] = -Vertex[1]
        Vertex[1] = tmpValue
        return
    

    def ConvertFile(self, filename):
        OutputFilename = filename[:-5] + ".obj"
        OutMatFilename = filename[:-5] + ".mtl"
        ScaleFactor = 1    # use 0.01 to export to blender
        
        # Everything is fine, Open the json file
        file = open(filename)
        Data = json.loads(file.read())

        # Close file
        file.close()

        #################################
        # Start Procesing the json data
        #################################
        
        # Metadata
        Metadata = Data["meta"]
        Alto = Metadata["size"]["h"]
        Ancho = Metadata["size"]["w"]

        # Output File
        OutputFile = open(OutputFilename, "w+")

        # IndexCache
        FaceIndexOffset = 0

        # Material
        OutMatFile = open(OutMatFilename, "w+")
        OutMatFile.write("newmtl TextureMap\nmap_Ka " + Metadata["image"] + "\nmap_Kd " + Metadata["image"])
        OutMatFile.close()
        
        OutputFile.write("mtllib " + OutMatFilename + "\n")
        OutputFile.write("usemtl TextureMap\n")

        # Frames
        Frames = Data["frames"]
        for FrameData in Frames:
            # DataFrame
            try:
                pivot = FrameData["pivot"]
            except :
                pivot = json.loads("{\"x\":0.5,\"y\":0.5}")

            sourceSize = FrameData["sourceSize"]
            rotated = FrameData["rotated"]
            OffsetSpriteWidth = sourceSize["w"] * pivot["x"]
            OffsetSpriteHeigth = sourceSize["h"] * pivot["y"]

            OutputFile.write("g " + FrameData["filename"] + "\n")

            for VertexData in FrameData["vertices"]:
                # Append an aditional value to the array
                VertexData.append(0)
                # Moves the Y value to Z (because UE4)
                VertexData[2] = VertexData[1]
                VertexData[1] = 0

                VertexData[0] = (VertexData[0] - OffsetSpriteWidth) * ScaleFactor
                VertexData[2] = (VertexData[2] - OffsetSpriteHeigth) * ScaleFactor

                self.RotateVertexXY(VertexData)
                self.RotateVertexXY(VertexData)

                # Frame Rotated?
                if rotated:
                    self.RotateVertexXY(VertexData)
                    
                self.RotateVertexXZ(VertexData)
                self.RotateVertexXZ(VertexData)

                # Write Vertex Data
                OutputFile.write("v " + str(VertexData[0]) + " " + str(-VertexData[1]) + " " + str(VertexData[2]) + "\n")

            for VertexUVData in FrameData["verticesUV"]:
                VertexUVData[0] /= Ancho
                VertexUVData[1] /= Alto

                # Write VertexUV Data
                OutputFile.write("vt " + str(VertexUVData[0]) + " " + str(1-VertexUVData[1]) + "\n")

            OutputFile.write("s off\n")

            #f v/vt/vn v/vt/vn v/vt/vn v/vt/vn
            for FaceData in reversed(FrameData["triangles"]):
                OutputFile.write("f " + str(FaceData[2] +FaceIndexOffset +1) + "/" + str(FaceData[2] +FaceIndexOffset +1) + "/" + " " + str(FaceData[1] +FaceIndexOffset +1) + "/" + str(FaceData[1] +FaceIndexOffset +1) + "/" + " " + str(FaceData[0] +FaceIndexOffset +1) + "/" + str(FaceData[0] +FaceIndexOffset +1) + "/" + "\n")

            FaceIndexOffset += len(FrameData["vertices"])

        OutputFile.close()
    

app = QtWidgets.QApplication([])
application = mywindow()
application.show()
sys.exit(app.exec())