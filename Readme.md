# **Spritesheet to OBJ converter**

This little app is used to convert Spritesheets created with [TexturePacker](https://www.codeandweb.com/texturepacker) with json format.

At the moment, to run the app you have to do it from commandline

```
python App.py
```


The app was only tested with Python-3.6.7.
It was made using PyQt5 (5.12).

Programmed by:
 - Jesús Mastache Caballero (BrunoCooper17)

[Portafolio](https://brunocooper17.gitlab.io/portafolio/projects)
[Twitter](https://twitter.com/BrunoCooper_17)
[LinkedIn](https://www.linkedin.com/in/jesus-mastache-25265952/)
